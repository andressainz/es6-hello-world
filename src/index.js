import { getLastname, getName } from './foo.js'

var express = require('express');
var app = express();

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send('hello world');
});

app.get('/profile', function(req, res) {
  res.send('hello profile ' + getName() + ' ' + getLastname());
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
